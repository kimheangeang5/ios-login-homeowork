//
//  ViewController.swift
//  login
//
//  Created by Kimheang on 11/9/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func loginTap(_ sender: Any) {
        if loginButton.titleLabel?.text == "Logout" {
            welcomeLabel.text = "Please login to use"
            loginButton.setTitle("Login", for: [])
            loginButton.backgroundColor = UIColor.blue
        }
        else{
            guard  let loginView = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController else {
                fatalError("View Controller not found")
            }
            loginView.delegate = self
            navigationController?.pushViewController(loginView, animated: true)        }
    }
    
}
extension ViewController : userInfoDelegate{
    func welcomeUserName(userName: String) {
        loginButton.setTitle("Logout", for: [])
        loginButton.backgroundColor = UIColor.red
        welcomeLabel.text = "Welcome \(userName)"
    }
}

