//
//  LoginViewController.swift
//  login
//
//  Created by Kimheang on 11/9/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
protocol userInfoDelegate: AnyObject{
    func welcomeUserName(userName: String)
}
class LoginViewController: UIViewController {
    weak var delegate: userInfoDelegate?
    static var userInformation = [User]()
    @IBOutlet weak var userNameErrLabel: UILabel!
    @IBOutlet weak var passwordErrLabel: UILabel!
    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginTap(_ sender: Any) {
        if LoginViewController.userInformation.count > 0 {
            if userNameText.text?.isEmpty == false{
                for i in 0..<LoginViewController.userInformation.count{
                    if LoginViewController.userInformation[i].userName == userNameText.text{
                        userNameErrLabel.text = ""
                        if passwordText.text?.isEmpty == false {
                            if LoginViewController.userInformation[i].password == passwordText.text{
                                self.navigationController?.popViewController(animated: true)
                                self.delegate?.welcomeUserName(userName: userNameText.text!)
                            }
                            else{
                                passwordErrLabel.text = "* Password is not correct!"
                            }
                        }
                        else{
                            passwordErrLabel.text = "* Password must be provide!"
                        }
                    }
                    else{
                        userNameErrLabel.text = "* Account not found!"
                        passwordErrLabel.text = ""
                    }
                }
            }
            else{
                if passwordText.text?.isEmpty == false{
                    userNameErrLabel.text = "* Username must be provide!"
                }
                else{
                    userNameErrLabel.text = "* Please insert username and password"
                }
            }
        }
        else{
            userNameErrLabel.text = "* No user sign up yet!"
        }
        
    }
    
    @IBAction func signUpTap(_ sender: Any) {
        guard  let signUpView = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController else {
            fatalError("View Controller not found")
        }
        signUpView.delegate = self
        navigationController?.pushViewController(signUpView, animated: true)
    }
}
extension LoginViewController: addNewUserDelegate{
    func addNewUser(_ name: String, _ password: String) {
        userNameErrLabel.text = ""
        passwordErrLabel.text = ""
        userNameText.text = ""
        passwordText.text = ""
        let user = User()
        user.userName = name
        user.password = password
        LoginViewController.userInformation.append(user)
    }
}
