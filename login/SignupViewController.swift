//
//  SignUpViewController.swift
//  login
//
//  Created by Kimheang on 11/9/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
protocol addNewUserDelegate: AnyObject {
    func addNewUser(_ name: String, _ password: String)
}
class SignUpViewController: UIViewController {
    weak var delegate: addNewUserDelegate?
   
    @IBOutlet weak var confirmPasswordErrLabel: UILabel!
    @IBOutlet weak var passwordErrLabel: UILabel!
    @IBOutlet weak var userNameErrLabel: UILabel!
    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func signupTap(_ sender: Any) {
        if userNameText.text?.isEmpty == false{
            if password.text?.isEmpty == false && confirmPassword.text?.isEmpty == false{
                if password.text == confirmPassword.text{
                    self.navigationController?.popViewController(animated: true)
                    delegate?.addNewUser(userNameText.text!, password.text!)
                }
                else{
                    confirmPasswordErrLabel.text = "* Password does not match!"
                }
            }
            if password.text?.isEmpty == true{
                passwordErrLabel.text = "* Password must be provide!"
            }
            else{
                passwordErrLabel.text = ""
            }
            if confirmPassword.text?.isEmpty == true {
                confirmPasswordErrLabel.text = "* Confirm password must be provide!"
            }
            userNameErrLabel.text = ""
        }
        else{
            if passwordErrLabel.text?.isEmpty == false{
                userNameErrLabel.text = "* Username must be provide!"
            }
            else{
                userNameErrLabel.text = "* Please insert username and password"
            }
        }
        
    }
    
}
